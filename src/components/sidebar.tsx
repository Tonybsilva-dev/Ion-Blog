import * as S from "../styles/components/sidebar";
import Image from "next/image";
import {
	AiOutlineInstagram,
	AiOutlineGithub,
	AiFillLinkedin,
	AiOutlineTwitter,
	AiFillHome,
	AiFillRead,
	AiOutlineGitlab
} from "react-icons/ai";
import { RiSendPlaneFill } from "react-icons/ri";
import { Button } from "../styles/components/button";
import { Divider } from "../styles/components/divider";
import Link from "next/link";
import { useRouter } from "next/router";
import { IoMdClose } from "react-icons/io";
import { useContext } from "react";
import { SidebarContext } from "../context/sidebarContext";

const Sidebar = () => {
	const { sidebarIsClose, setSidebarIsClose } = useContext(SidebarContext);
	const router = useRouter();

	return (
		<S.Container close={sidebarIsClose}>
			<IoMdClose
				className="btn-close-sidebar"
				onClick={() => setSidebarIsClose(!sidebarIsClose)}
			/>

			<h2>Íon</h2>

			<img src="/assets/Tony.jpeg" alt="Picture of the author" />

			<h4>SOFTWARE ENGINEER</h4>
			<Divider width={75} />
			<section className="container-social-networks">
				<a
					href="https://instagram.com/tonybsilva"
					target="__blank"
					rel="noopener"
				>
					<AiOutlineInstagram />
				</a>

				<a href="https://gitlab.com/Tonybsilva-dev" target="__blank" rel="noopener">
					<AiOutlineGitlab />
				</a>

				<a
					href="https://www.linkedin.com/in/tony-silva/"
					target="__blank"
					rel="noopener"
				>
					<AiFillLinkedin />
				</a>

				<a href="https://twitter.com/tonybsilvadev" target="__blank" rel="noopener">
					<AiOutlineTwitter />
				</a>
			</section>

			<Divider width={75} />

			<nav>
				<Link href="/">
					<a className={`${router.asPath === "/" ? "active" : ""} link`}>
						<AiFillHome />
						Inicio
					</a>
				</Link>
				<Link href="/blog">
					<a className={`${router.asPath === "/blog" ? "active" : ""} link`}>
						<AiFillRead />
						Posts
					</a>
				</Link>

				<Button
					as="a"
					href="https://api.whatsapp.com/send?phone=5583987308551"
					target="__blank"
					rel="noopener"
					width={160}
					height={45}
					primary
					className="btn-callme"
				>
					<RiSendPlaneFill />
					Enviar mensagem
				</Button>
			</nav>
		</S.Container>
	);
};

export default Sidebar;
