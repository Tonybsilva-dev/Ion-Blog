import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  :root {
    // Box Shadow
    --box-shadow: #00000030;
    --hover-shadow: #00000080;

    // Color
    --primary-color: #1B2021;
    --secondary-color: #145DA0;
    --light-color: #2dd881;
    --dark-color: #FFF;
    --muted-color: #858585;
    --row-color: #848fa5;

    // Buttons
    --button-primary: #FFF;
    --hover-primary: #0e4171;
    --button-secondary: #FFF;
    --hover-secondary: #145DA0;
  }

  .post {
    text-decoration: none;
  }

  body {
    background: var(--dark-color);
  }
`;

export default GlobalStyle;
